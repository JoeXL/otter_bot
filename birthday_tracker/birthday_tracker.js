const basic_responses = require("./basic_responses.js")

/*
	setBirthdayChat		- (chat_link)
	setMemberBirthday	- (player_link, date)
	setBirthdayRole		- (role_link)
	*/

/* Params: chat_link */
function set_birthday_chat(received_message)
{

}

/* Params: player_link, date */
function set_member_birthday(received_message)
{

}

/* Params: role_link */
function set_birthday_role(received_message)
{

}

exports.setBirthdayChat = setBirthdayChat
exports.setMemberBirthday = setMemberBirthday
exports.setBirthdayRole = setBirthdayRole