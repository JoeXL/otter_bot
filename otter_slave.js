const Discord = require('discord.js')
const client = new Discord.Client()

const commands = require('./common/commands.js')
const basic_responses = require('./common/basic_responses.js')

var set_client = false

client.on('message', (received_message) => {
	if (received_message.author == client.user) { // Prevent bot from responding to its own messages
		return
	}

	if (received_message.content.startsWith(commands.command_prefix)) {
		process_command(received_message)
	}
})

client.on('ready', () => {
	//load data
})

function process_command(received_message) {
	let full_command = received_message.content.substr(commands.command_prefix.length) // Remove the leading exclamation mark
	let split_command = full_command.split(" ") // Split the message up in to pieces for each space
	let primary_command = split_command[1] // The first word directly after the exclamation is the command
	let arguments = split_command.slice(2) // All other words are arguments/parameters/options for the command

	console.log("Command received: " + primary_command)
	console.log("Arguments: " + arguments) // There may not be any arguments

	var command_function = commands.commands_list[primary_command]
	if(command_function) {
		command_function(arguments, received_message)
	}
	else {
		basic_responses.invalid_command(arguments, received_message)
	}
}

client.login("NDQwODU0MTEzMjMyNzQ4NTQ0.XS9qFg.-Thai7DbALDw5CoLy6bBzBGcPks") // Replace XXXXX with your bot token