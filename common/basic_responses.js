function respond_help(arguments, received_message) {
	if (arguments.length > 0) {
		received_message.channel.send("It looks like you might need help with " + arguments)
	} else {
		received_message.channel.send("I'm not sure what you need help with. Try `!help [topic]`")
	}
	arguments.length > 0
		? received_message.channel.send("It looks like you might need help with " + arguments)
		: received_message.channel.send("I'm not sure what you need help with. Try `!help [topic]`")
}

function invalid_command(arguments, received_message) {
	received_message.channel.send("Sorry I don't understand that command :(");
}

exports.respond_help = respond_help
exports.invalid_command = invalid_command