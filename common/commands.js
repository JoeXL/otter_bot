const basic_responses = require("./basic_responses.js")
const birthday_tracker = require("../birthday_tracker/birthday_tracker.js")
//const data_io = require("./data_io.js")

const local_command_prefix = "!O"

const local_commands_list = {
	"help" : basic_responses.respond_help,
	
	/*
	setBirthdayChat		- (chat_link)
	setMemberBirthday	- (player_link, date)
	setBirthdayRole		- (role_link)
	*/

	"set_birthday_chat" : birthday_tracker.set_birthday_chat,
	"set_member_birthday" : birthday_tracker.set_member_birthday,
	"set_birthday_role" : birthday_tracker.set_birthday_role
}

exports.commands_list = local_commands_list
exports.command_prefix = local_command_prefix